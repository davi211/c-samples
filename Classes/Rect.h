#pragma once
class Rect
{
public:
	// constructors
	Rect(void);
	Rect(float height, float width);

	// methods
	float getArea(void);
	float getPerimeter(void);

	virtual ~Rect(void);	// destructor

	// attributes
	float height;
	float width;
};

