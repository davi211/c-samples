#include "Rect.h"

// constructor
// inizialise all attributes with neutral value
// numbers with 0
// bool with false
// pointer with nullptr
Rect::Rect(void)
{
	height = 0;
	width = 0;
}

Rect::Rect(float h, float w)
{
	height = h;
	width = w;
}

float Rect::getArea(void)
{
	return height * width;
}

float Rect::getPerimeter(void)
{
	return (height + width) * 2;
}

Rect::~Rect(void)
{
}
