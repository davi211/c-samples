#include <iostream>
#include "Rect.h"

using namespace std;

int main(void)
{
	// NON-POINTER WAY

	Rect rectang = Rect();

	rectang.height = 9.2;
	rectang.width = 2.5;

	cout << rectang.getArea() << endl;
	cout << rectang.getPerimeter() << endl;



	// POINTER WAY

	// Instantiate a new rectangle (allocate some memory to hold a rectangle)
	Rect *rectangle = new Rect(9.2, 2.5);

	cout << rectangle->getArea() << endl;
	cout << rectangle->getPerimeter() << endl;

	// release allocated memory
	delete rectangle;
	rectangle = nullptr;

	return 0;
}