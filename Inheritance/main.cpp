#include <iostream>
#include "Generic.h"
#include "Special.h"

using namespace std;

int main (void)
{
	Generic *test1 = new Generic();
	test1->printSomething();			// prints "Generic"
	delete test1;
	test1 = nullptr;

	Special *test2 = new Special();
	test2->printSomething();			// prints "Specialized"
	delete test2;
	test2 = nullptr;

	Generic *test3 = new Special();
	test3->printSomething();			// prints "Specialized"
	delete test3;
	test3 = nullptr;

	//Special *test4 = new Generic();	// not valid as Generic is not a Special

	return 0;
}