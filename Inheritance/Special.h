#pragma once
#include "Generic.h"

class Special : public Generic
{
public:
	Special(void);

	void printSomething(void);

	virtual ~Special(void);
};

