// http://www.cplusplus.com/reference/vector/vector/

#include <iostream>
#include <vector>

using namespace std;

int main(void)
{
	vector<int> myVector;

	// Insert element at the end
	myVector.push_back(12);		// myVector = [12]

	myVector.push_back(23);		// myVector = [12, 23]

	myVector.push_back(34);		// myVector = [12, 23, 34]

	// Access element by index
	myVector[1] = 5;			// myVector = [12, 5, 34]

	// Remove element at index
	myVector.erase(myVector.begin() + 1);	// myVector = [12, 34]

	// Remove last element
	myVector.pop_back();		// myVector = [12]

	myVector.push_back(34);
	myVector.push_back(56);
	myVector.push_back(78);		// myVector = [12, 34, 56, 78]

	// Iterate all elements
	for(auto it = myVector.begin(); it != myVector.end(); ++it)
	{
		int &currentElement = *it;

		cout << currentElement << endl;
	}

	// Removing elements while inside a loop
	auto it = myVector.begin();
	while(it != myVector.end())
	{
		int &currentElement = *it;

		if(currentElement == 56)	// Remove element "56"
		{
			it = myVector.erase(it);
		}
		else
		{
			++it;
		}
	}
	
	// myVector = [12, 34, 78]

	// Adding elements while inside a loop
	it = myVector.begin();
	while(it != myVector.end())
	{
		int &currentElement = *it;

		if(currentElement == 12)	// Add element "56" after "12"
		{
			it = myVector.insert(it+1, 56);
		}

		++it;
	}

	// myVector = [12, 56, 34, 78]

	return 0;
}