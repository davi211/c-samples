#include <iostream>
#include <cmath>

using namespace std;

int main(void)
{
	int variable = 5;				// variable initialized with value 5

	cout << variable << endl;		// print variable: 5

	int *pointer = nullptr;			// pointer pointing to null

	cout << pointer << endl;		// print pointed address: 0

	pointer = &variable;			// pointer pointing to variable (pointer assigned address of variable)
	
	cout << &variable << endl;		// print address of variable 

	cout << pointer << endl;		// print pointed addres: address of variable

	cout << *pointer << endl;		// print pointed value: 5

	cout << pointer[0] << endl;		// print value at the address in position 0: 5

	*pointer = 6;					// assign 6 to the cell pointed by pointer (assign 6 to variable)

	cout << *pointer << endl;		// print pointed value: 6

	cout << variable << endl;		// print variable: 6

	cout << pointer[0] << endl;		// print value at the address in position 0: 6

	pointer[0] = 7;					// change variable value to 7

	cout << *pointer << endl;		// print pointed value: 7 

	cout << variable << endl;		// print variable: 7

	cout << pointer[0] << endl;		// print value at the address in position 0: 7


	return 0;
}